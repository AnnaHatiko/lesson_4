const io = require('socket.io')(server);
var request = require('request');
const port = process.env.PORT || 3000;
var options = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160723T183155Z.f2a3339517e26a3c.d86d2dc91f2e374351379bb3fe371985273278df&text=';

server.listen(port, function() {
    console.log('Server listening at', port);
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {

    socket.on('request yandex', function(data) {
        var url = options + data.message + '&lang=ru-en';
        request({
                encoding: null,
                url: url,
                method: "POST",
                json:true},function(error,response,body){
                socket.emit('show result', body.text);
            }
        );
    });

});